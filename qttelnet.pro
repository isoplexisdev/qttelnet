QT += network

QMUTIL_DIR = "$$cat($$shadowed($$_FILE_"."QMUTIL_DIR.qm.tmp))"
include($${QMUTIL_DIR}/qm.module_macros.pri)

include($$dirToModuleFile($$PWD))

TEMPLATE = lib
CONFIG += c++17

include(common.pri)
include(src/qttelnet.pri)

TARGET = $$QTTELNET_LIBNAME

DetectAndAddQtRunDepsPostLink($${TARGET}.dll)
#fancy_deploy($${TARGET}.dll)
