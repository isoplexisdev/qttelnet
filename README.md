# Telnet v2.1

A client for the telnet protocol.

## Code Source

Forked from [https://github.com/triochi/qttelnet-2.1-opensource] @ 5/1/20

This code can be found at [https://bitbucket.org/isoplexisdev/qttelnet]

## Licensing

Licensed under LGPL-2.1-only

See `LICENSE.LGPL`

## Notes
