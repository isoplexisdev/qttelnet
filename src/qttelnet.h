/*
* This file is part of QtTelnet.
*
* qttelnet.h
*   Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
*   Copyright (C) 2020 IsoPlexis, Inc.
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
*
* You should have received a copy of the GNU Lesser General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef QTTELNET_H
#define QTTELNET_H

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QSize>
#include <QtCore/QRegExp>
#include <QtNetwork/QTcpSocket>

class QtTelnetPrivate;

#if defined Q_OS_WIN
#  if defined(QT_QTTELNET_LIBRARY)
#    define QT_QTTELNET_EXPORT Q_DECL_EXPORT
#  else
#    define QT_QTTELNET_EXPORT Q_DECL_IMPORT
#  endif
#endif

class QT_QTTELNET_EXPORT QtTelnet : public QObject
{
    Q_OBJECT
    friend class QtTelnetPrivate;
public:
    QtTelnet(QObject *parent = 0);
    ~QtTelnet();

    enum Control { GoAhead, InterruptProcess, AreYouThere, AbortOutput,
                   EraseCharacter, EraseLine, Break, EndOfFile, Suspend,
                   Abort };

    void connectToHost(const QString &host, quint16 port = 23);
    bool isConnected();
    bool waitForConnected(int = 3000);  // ms
    bool isValid();

    void login(const QString &user, const QString &pass);

    void setWindowSize(const QSize &size);
    void setWindowSize(int width, int height); // In number of characters
    QSize windowSize() const;
    bool isValidWindowSize() const;

    void setSocket(QTcpSocket *socket);
    QTcpSocket *socket() const;

    void setPromptPattern(const QRegExp &pattern);
    void setPromptString(const QString &pattern)
    { setPromptPattern(QRegExp(QRegExp::escape(pattern))); }
public Q_SLOTS:
    void close();
    void logout();
    void sendControl(Control ctrl);
    void sendData(const QString &data);
    void sendSync();

Q_SIGNALS:
    void loginRequired();
    void loginFailed();
    void loggedIn();
    void loggedOut();
    void connectionError(QAbstractSocket::SocketError error);
    void message(const QString &data);

public:
    void setLoginPattern(const QRegExp &pattern);
    void setLoginString(const QString &pattern)
    { setLoginPattern(QRegExp(QRegExp::escape(pattern))); }
    void setPasswordPattern(const QRegExp &pattern);
    void setPasswordString(const QString &pattern)
    { setPasswordPattern(QRegExp(QRegExp::escape(pattern))); }

private:
    QtTelnetPrivate *d;
};
#endif
