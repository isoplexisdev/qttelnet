qttelnet-uselib {
    SOURCES += $$PWD/qttelnet.cpp
    HEADERS += $$PWD/qttelnet.h
    win32:LIBS += -lWs2_32
}

win32 {
    DEFINES += QT_QTTELNET_LIBRARY
}
