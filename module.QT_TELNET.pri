!contains(ALREADY_INCLUDED, $$_FILE_) {

    ALREADY_INCLUDED += $$_FILE_

    defineTest(MY_INIT) {

        MY_MODULE = $$UseModuleFile($$_FILE_)

        AddClientLinkItems($$getDestDir($${PWD})/"*.lib")
        AddClientRunFiles($$getDestDir($${PWD})/"*.dll")
		
        AddHeaderDirs($${PWD}/src)
    }

    MY_INIT()
    unset(MY_INIT)

}
